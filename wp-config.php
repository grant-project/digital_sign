<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('FS_METHOD', 'direct');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'npomars');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$H]=yYe[h/;jr[|}%^Vl`nVB8P4:!fGxO~heW--xk]%k<m,b3neNi$xOcIm!=Qif');
define('SECURE_AUTH_KEY',  'qGJ}Xk5r,D CAh?%E]i@yoY~7JIy:HgTg.;G:9C<;f=&?Z`,ePh-x6$X)_:<>x3%');
define('LOGGED_IN_KEY',    '~9P~n5vjg9I+HmT6.;B8{]f#9kK12C>KDl}J jgpCPL[w??VOb1KJ4?twVKq{2^V');
define('NONCE_KEY',        '?pb#]A91bsoRvik+$>p %B>@O`~F3kOBS#d6}Pshzq!,G/ppa;aH2r|s#N54Af5J');
define('AUTH_SALT',        '{f^,O2bn+RnMSZifwqxA.oB<I<=k8:NRi$$R,K2/l3p@DuDKG4w]B([zM^.T5/O*');
define('SECURE_AUTH_SALT', '>C4yhlm7w%ubK0!g)<f2N B%7$f8k(!8[iK%UL[&+_c}69V7Gf@rU[OD${;bQd*R');
define('LOGGED_IN_SALT',   '>mA`Gb1o@wewVo9C-6yA/flMEa02Y.<Iy6DpTa&z+4&K@D$Z}I=9#!ixI7`_?-pr');
define('NONCE_SALT',       '!?M%:vOn_(OvKf:wm4Gxxu(->c``>%m|gZ/Q3XD](i_C(xnN]JZv+)e~^ &`u(n4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

